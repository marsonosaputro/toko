<?php

//frontend
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/produk/{slug?}', 'HomeController@produk');
Route::get('/artikel/{slug?}', 'HomeController@artikel');
Route::get('/cara-belanja', 'HomeController@caraBelanja');
Route::get('/kontak', 'HomeController@kontak');

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:administrator']], function()
{
  Route::get('dashboard', 'HomeController@index');
  Route::resource('artikel', 'ArtikelController');
  Route::resource('user', 'UserController');
  Route::resource('statis', 'StatisController');
  Route::resource('banner', 'BannerController');
  Route::resource('slideshow', 'SlideshowController');
  Route::resource('produk', 'ProdukController');
  Route::resource('kategori', 'KategoriController');
  Route::resource('identitas', 'IdentitasController');
});

Auth::routes();
Route::get('/home', 'HomeController@index');

//VIEW
View::composer(['frontend', 'frontend.sidebar'], function($view){
  $view->kategoriproduk = App\Kategori::all();
  $view->identitas = App\Identitas::where('id',1)->first();
});
