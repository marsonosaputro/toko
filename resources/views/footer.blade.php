<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Support by: <b><a href="https://facebook.com/marsonosaputro">Marsono Saputro</a></b>
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2017 <a href="#">Marsono Saputro</a>.</strong> All rights reserved.
</footer>
