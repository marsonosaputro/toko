@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Artikel</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'artikel.store', 'files'=>true, 'class' => 'form-horizontal']) !!}
          @include('artikel._form')
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
