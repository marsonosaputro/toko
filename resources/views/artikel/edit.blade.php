@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Artikel</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($artikel, ['route' => ['artikel.update', $artikel->id], 'files' => true, 'class' => 'form-horizontal', 'method' => 'PUT']) !!}

          @include('artikel._form')

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
