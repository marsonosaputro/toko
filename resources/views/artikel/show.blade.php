@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Detail Artikel</h3>
    </div>
    <div class="panel-body">
      <h3>{{ $artikel->judul }}</h3>
      <h5>
          <i class="fa fa-user"></i> Penulis: {{ $artikel->user->name }} &nbsp; &nbsp; &nbsp;
          <i class="fa fa-dashboard"></i> Hits: {{ $artikel->hits }} &nbsp; &nbsp; &nbsp;
          <i class="fa fa-calendar"></i> Posting : {{ $artikel->created_at }}
      </h5>
      <hr>
      @if (!empty($artikel->image))
        <img src="{{ asset('images/artikel/'.$artikel->image) }}" class="img img-responsive img-thumbnail" style="float:left; margin-right: 10px; margin-bottom: 10px;" />
      @endif
      {!! $artikel->deskripsi !!}
      <hr>
      <div class="pull-right">
        <div class="btn-group">
          <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-backward"></i> KEMBALI</a>
          <a href="{{ route('artikel.edit', $artikel->id) }}" class="btn btn-info"><i class="fa fa-edit"></i> EDIT</a>
        </div>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
