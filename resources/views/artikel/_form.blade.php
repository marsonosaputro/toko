@include('tinymce')
<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul Artikel', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
    {!! Form::label('deskripsi', 'Deskripsi / Isi', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('deskripsi', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
    </div>
</div>
@if (!empty($artikel->image))
  <div class="form-group">
      {!! Form::label('inputname', 'Gambar sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
              <img src="{{ asset('images/artikel/'.$artikel->image) }}" class="img img-responsive img-thumbnail" />
          </div>
  </div>
@endif
<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Gambar', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('image', ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
</div>

{!! Form::hidden('hits', 0) !!}
{!! Form::hidden('user_id', Auth::user()->id) !!}

<div class="btn-group pull-right">
    <a href="{{ route('artikel.index') }}" class="btn btn-warning">Batal</a>
    {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
</div>
