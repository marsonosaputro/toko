@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Artikel</h3>
    </div>
    <div class="panel-body">
      @include('notif')
      <a href="{{ route('artikel.create') }}" class="btn btn-primary" style="margin-bottom: 10px;'">Tambah</a>

      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>Hits</th>
              <th>Penulis</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($artikel as $d)
              {!! Form::open(['method' => 'delete', 'route' => ['artikel.destroy', $d->id]]) !!}
                  {!! Form::hidden('_delete', 'DELETE') !!}
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $d->judul }}</td>
                    <td>{{ $d->hits }}</td>
                    <td>{{ $d->user->name }}</td>
                    <td>
                      <div class="btn-group">
                        <a href="{{ route('artikel.show', $d->id) }}" class="btn btn-sm btn-success"><i class="fa fa-folder-open"></i> VIEW</a>
                        <a href="{{ route('artikel.edit', $d->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> EDIT</a>
                        <button type="submit" onclick="javascript: return confirm('Yakin akan dihapus?')" class="btn btn-sm btn-danger" name="button"><i class="fa fa-trash-o"></i> HAPUS</button>
                      </div>
                    </td>
                  </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="pull-right" style="margin-top: -20px;">
        {!! $artikel->render() !!}
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
