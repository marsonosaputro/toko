<div class="col-md-12 no-padding">
    <div class="pull-right visible-lg">
        <ul id="hornavmenu" class="nav navbar-nav">
            <li>
                <a href="index.html" class="fa-home">Home</a>
            </li>
            <li>
                <span class="fa-gears">Kegiatan</span>
                <ul>
                    <li><a href="#">Seminar Keluarga</a></li>
                    <li><a href="#">Berita Talkshow</a></li>
                    <li><a href="#">Gallery Kegiatan</a></li>
                </ul>
            </li>
            <li>
                <span class="fa-copy">Pages</span>
                <ul>
                    <li><a href="#">Profil JKI</a></li>
                    <li><a href="#">Profil Kak AdhiPASTI</a></li>
                    <li><a href="pages-faq.html">F.A.Q.</a></li>
                    <li><a href="pages-about-me.html">About Me</a></li>
                </ul>
            </li>
            <li>
                <span class="fa-th">Testimony</span>
                <ul>
                    <li><a href="#">Seminar</a></li>
                    <li><a href="">TJB</a></li>
                </ul>
            </li>
            <li>
                <span class="fa-font">Berita</span>
                <ul>
                    <li>
                        <a href="blog-list.html">Blog</a>
                    </li>
                    <li>
                        <a href="blog-single.html">Blog Single Item</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="contact.html" class="fa-comment">Contact</a>
            </li>
        </ul>
    </div>
</div>
