@extends('frontend')
@section('content')
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/produk') }}">Produk</a></li>
                        <li>{{ $detail->judul }}</li>
                    </ul>

                </div>

                <div class="col-md-3">
                    <!-- SIDEBAR -->
                    @include('frontend.sidebar')
                </div>

                <div class="col-md-9">

                    <div class="row" id="productMain">
                        <div class="col-sm-6">
                            <div id="mainImage">
                                <img src="{{ asset('images/produk/'.gambar($detail->id)) }}" alt="" class="img-responsive">
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="box">
                                <h1 class="text-center">{{ $detail->produk }}</h1>
                                <p class="text-center"><b>Stock tersedia : {{ $detail->stock }}</b></p>
                                <p class="goToDescription"><a href="#details" class="scroll-to">Detail</a>
                                </p>
                                <p class="price">Rp {{ number_format($detail->harga,0,',','.') }},-</p>

                                <p class="text-center buttons">
                                  <a href="{{ url('/product/cart/'.$detail->id) }}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Beli</a>
                                </p>
                            </div>
                            <div class="row" id="thumbs">
                              @foreach ($detail->gambarproduk as $key => $d)
                                <div class="col-xs-4">
                                    <a href="{{ asset('images/produk/'.$d->gambar) }}" class="thumb">
                                        <img src="{{ asset('images/produk/thumb/'.$d->gambar) }}" alt="" class="img-responsive">
                                    </a>
                                </div>
                              @endforeach
                            </div>
                        </div>

                    </div>


                    <div class="box" id="details">
                        {!! $detail->deskripsi !!}
                            <hr>
                            <div class="social">
                                <h4>Bagikan ke</h4>
                                <p>
                                    <!-- AddToAny BEGIN -->
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                    <a class="a2a_button_whatsapp"></a>
                                    <a class="a2a_button_line"></a>
                                    <a class="a2a_button_sms"></a>
                                    </div>
                                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                                    <!-- AddToAny END -->
                                </p>
                            </div>
                    </div>

                    <div class="row same-height-row">
                        <div class="col-md-3 col-sm-6">
                            <div class="box same-height">
                                <h3>Produk Terbaru</h3>
                            </div>
                        </div>
                        @foreach($terbaru as $key => $d)
                            <div class="col-md-3 col-sm-6">
                                <div class="product same-height">
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front">
                                                <a href="{{ url('produk/'.$d->slug) }}">
                                                  <img src="{{ URL::asset('/images/produk/' . gambar($d->id)) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="back">
                                                <a href="{{ url('produk/'.$d->slug) }}">
                                                  <img src="{{ URL::asset('/images/produk/' . gambar2($d->id)) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ url('produk/'.$d->slug) }}" class="invisible">
                                        <img src="{{ URL::asset('/images/produk/' . gambar($d->id)) }}" alt="" class="img-responsive">
                                    </a>
                                    <div class="text">
                                        <h3>{{ $d->produk }}</h3>
                                        <p class="price">Rp {{ number_format($d->harga,0,',','.') }},-</p>
                                    </div>
                                </div>
                                <!-- /.product -->
                            </div>
                        @endforeach

                    </div>

                    <div class="row same-height-row">
                        <div class="col-md-3 col-sm-6">
                            <div class="box same-height">
                                <h3>Produk paling dicari</h3>
                            </div>
                        </div>
                        @foreach($hits as $key => $d)
                            <div class="col-md-3 col-sm-6">
                                <div class="product same-height">
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front">
                                                <a href="{{ url('produk/'.$d->slug) }}">
                                                  <img src="{{ URL::asset('/images/produk/' . gambar($d->id)) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="back">
                                                <a href="{{ url('produk/'.$d->slug) }}">
                                                    <img src="{{ URL::asset('/images/produk/' . gambar2($d->id)) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ url('produk/'.$d->slug) }}" class="invisible">
                                        <img src="{{ URL::asset('/images/produk/' . gambar($d->id)) }}" alt="" class="img-responsive">
                                    </a>
                                    <div class="text">
                                        <h3>{{ $d->produk }}</h3>
                                        <p class="price">Rp {{ number_format($d->harga,0,',','.') }},-</p>
                                    </div>
                                </div>
                                <!-- /.product -->
                            </div>
                        @endforeach

                    </div>

                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@endsection
