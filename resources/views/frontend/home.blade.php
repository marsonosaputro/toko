@extends('frontend')
@section('content')
    <div id="all">

        <div id="content">

            <div class="container">
                <div class="col-md-12">
                    <div id="main-slider">
                        @foreach($slideshow as $key => $d)
                            <div class="item">
                                <img class="img-responsive" src="{{ asset('images/slideshow/'.$d->image) }}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <!-- /#main-slider -->
                </div>
            </div>

            <!-- *** HOT PRODUCT SLIDESHOW ***
    _________________________________________________________ -->
            <div id="hot">

                <div class="box">
                    <div class="container">
                        <div class="col-md-12">
                            <h2>Produk Terbaru Kami</h2>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="product-slider">
                        @foreach($produk as $key => $d)
                        <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="{{ url('produk/'.$d->slug) }}">
                                                <img src="{{ asset('images/produk/'.gambar($d->id)) }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="{{ url('produk/'.$d->slug) }}">
                                                <img src="{{ asset('images/produk/'.gambar2($d->id)) }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('produk/'.$d->slug) }}" class="invisible">
                                    <img src="{{ asset('images/produk/'.gambar($d->id)) }}" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="{{ url('produk/'.$d->slug) }}">{{ $d->produk }}</a></h3>
                                    <p class="price">Rp {{ number_format($d->harga,0,'.',',') }}</p>
                                </div>
                                <!-- /.text -->

                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.col-md-4 -->
                        @endforeach

                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.container -->

            </div>
            <!-- /#hot -->

            <!-- *** HOT END *** -->

            <!-- *** BLOG HOMEPAGE ***
    _________________________________________________________ -->

            <div class="box text-center" data-animate="fadeInUp">
                <div class="container">
                    <div class="col-md-12">
                        <h3 class="text-uppercase">Yang baru di artikel</h3>

                        <p class="lead">Artikel apa yang baru di Butik Anis? <a href="{{ url('artikel') }}">Cek di sini!</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="container">

                <div class="col-md-12" data-animate="fadeInUp">

                    <div id="blog-homepage" class="row">
                        @foreach($artikel as $key => $d)
                            <div class="col-sm-6">
                                <div class="post">
                                    <h4><a href="#">{{ $d->judul }}</a></h4>
                                    <p class="author-category">Oleh: <a href="#">{{ $d->user->name }}</a>
                                    </p>
                                    <hr>
                                    <p class="intro">{!! str_limit($d->deskripsi, 250) !!}</p>
                                    <p class="read-more"><a href="{{ url('artikel/'.$d->slug) }}" class="btn btn-primary">Selengkapnya</a>
                                    </p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <!-- /#blog-homepage -->
                </div>
            </div>
            <!-- /.container -->

            <!-- *** BLOG HOMEPAGE END *** -->


        </div>
        <!-- /#content -->

@endsection
