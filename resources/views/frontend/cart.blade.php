@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-md-12">
    <ul class="breadcrumb">
    <li><a href="#">Home</a>
    </li>
    <li>Keranjang Belanja</li>
    </ul>
    </div>
    <div class="col-md-9" id="basket">
    <div class="box">
    <h1>Keranjang Belanja</h1>
    <p class="text-muted">Anda mempunyai {{ Cart::count() }} barang di keranjang.</p>
    {!! Form::open(['method' => 'POST', 'url' => 'cart-update', 'class' => 'form-horizontal']) !!}
    <div class="table-responsive">
    <table class="table">
    <thead>
    <tr>
    <th colspan="2">Produk</th>
    <th>Jumlah Barang</th>
    <th>Harga</th>
    <th colspan="2">Total</th>
    </tr>
    </thead>
    <tbody>
@foreach($cart_content as $key => $d)
    <tr>
    <td>
    <a href="#">
    <img src="{{ asset('/gambar/produk/' . $d->options->img) }}" alt="{{ $d->name }}">
    </a>
    </a>
    </td>
    <td><a href="#">{{ $d->name }}</a>
    </td>
    <td>
    <input type="number" name="qty{{ $no++ }}" value="{{ $d->qty }}" class="form-control" style="width:30%">
    </td>
    <td>Rp {{ number_format($d->price, 0,'.','.') }}</td>
    <td>Rp {{ number_format($d->subtotal, 0,'.','.') }}</td>
    <td><a href="{{ url('cart/delete/'.$d->rowid) }}"><i class="fa fa-trash-o"></i></a>
    </td>
    </tr>
    {!! Form::hidden('id'.$id++, $d->rowid) !!}
@endforeach
    {!! Form::hidden('jenis', ($no-1)) !!}
    </tbody>
    <tfoot>
    <tr>
    <th colspan="5">Total Pembayaran</th>
    <th colspan="2">Rp {{ number_format(Cart::total(), 0,'.','.') }}</th>
    </tr>
    </tfoot>
    </table>
    </div>
                        <!-- /.table-responsive -->
    <div class="box-footer">
    <div class="pull-left">
    <a href="{{ url('/gallery-produk') }}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Lanjut Belanja</a>
    </div>
    <div class="pull-right">
    <button type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Update Keranjang</button>
    <a href="{{ url('cart/checkout') }}" class="btn btn-primary">Proses Pembayaran <i class="fa fa-chevron-right"></i></a>
    </div>
    </div>
    {!! Form::close() !!}
    </form>
    </div>
                <!-- /.box -->
    <div class="row same-height-row">
    <div class="col-md-3 col-sm-6">
    <div class="box same-height">
    <h3>Produk yang pasti Anda juga suka</h3>
    </div>
    </div>
@foreach($terbaru as $key => $d)
    <div class="col-md-3 col-sm-6">
    <div class="product same-height">
    <div class="flip-container">
    <div class="flipper">
    <div class="front">
    <a href="{{ url('detail-produk/'.$d->seo) }}">
    <img src="{{ URL::asset('/gambar/produk/' . $d->gambar) }}" alt="" class="img-responsive">
    </a>
    </div>
    <div class="back">
    <a href="{{ url('detail-produk/'.$d->seo) }}">
    <img src="{{ URL::asset('/gambar/produk/' . $d->gambar) }}" alt="" class="img-responsive">
    </a>
    </div>
    </div>
    </div>
    <a href="{{ url('detail-produk/'.$d->seo) }}" class="invisible">
    <img src="{{ URL::asset('/gambar/produk/' . $d->gambar) }}" alt="" class="img-responsive">
    </a>
    <div class="text">
    <h3>{{ $d->judul }}</h3>
    <p class="price">Rp {{ number_format($d->harga,0,'.',',') }}</p>
    </div>
    </div>
                            <!-- /.product -->
    </div>
@endforeach
    </div>
    </div>
            <!-- /.col-md-9 -->
    <div class="col-md-3">
    <div class="box" id="order-summary">
    <div class="box-header">
    <h3>Ringkasan Pesanan</h3>
    </div>
    <p class="text-muted">Berikut ringkasan pesanan Anda</p>
    <div class="table-responsive">
    <table class="table">
    <tbody>
    <tr>
    <td>Jumlah Barang</td>
    <th>{{ Cart::count() }} items</th>
    </tr>
    <tr>
    <td>Harga</td>
    <th>Rp{{ number_format(Cart::total(),0,',','.') }}</th>
    </tr>
    <tr class="total">
    <td>Total Pembayaran</td>
    <th>Rp{{ number_format(Cart::total(),0,',','.') }}</th>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    <div class="box">
    <div class="box-header">
    <h4>Rekening Pembayaran</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/bank 1.jpg" alt="sales 2014" class="img-responsive"><br>
    <img src="{{ asset('frontend/') }}/img/bank 2.jpg" alt="sales 2014" class="img-responsive">
    </div>
    <div class="box">
    <div class="box-header">
    <h4>Jasa Pengiriman Barang</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/JNE.jpg" alt="sales 2014" class="img-responsive">
    </div>
    </div>
            <!-- /.col-md-3 -->
    </div>
        <!-- /.container -->
    </div>


@endsection
