@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
      <div class="col-sm-12">
        <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="{{ url('/artikel') }}">Artikel</a></li>
        <li>{{ $artikel->judul }}</li>
        </ul>
      </div>
    <div class="col-sm-9" id="blog-post">
      <div class="box">
        <h1>{{ $artikel->judul }}</h1>
        <p class="author-date">Oleh: {{ $artikel->user->name }} | Pada: {{ tgl_indo($artikel->created_at) }} | Dibaca: {{ $artikel->hits }} kali</p>
        <hr>
        <div id="post-content">
          <img src="{{ asset('images/artikel/'.$artikel->image) }}" style="float:left; margin: 0 10px 10px 0;" class="img-responsive">
          {!! $artikel->deskripsi !!}
          <hr>
          <h4>Bagikan ke:</h4>
                          <!-- AddToAny BEGIN -->
          <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_google_plus"></a>
            <a class="a2a_button_whatsapp"></a>
            <a class="a2a_button_line"></a>
            <a class="a2a_button_sms"></a>
          </div>
          <script async src="https://static.addtoany.com/menu/page.js"></script>
                          <!-- AddToAny END -->
          </p>
        </div>
                        <!-- /#post-content -->
        <div id="comment-form" data-animate="fadeInUp">
        </div>
                        <!-- /#comment-form -->
      </div>
                <!-- /.box -->
    </div>
            <!-- /#blog-post -->
    <div class="col-md-3">
                <!-- *** BLOG MENU ***
_________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">
      <div class="panel-heading">
        <h3 class="panel-title">Kategori Artikel</h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
          <li>judul</li>
          <li>judul</li>
        </ul>
      </div>
    </div>
                <!-- /.col-md-3 -->
                <!-- *** BLOG MENU END *** -->
    <div class="box">
    <div class="box-header">
    <h4>Rekening Pembayaran</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/bank 1.jpg" alt="sales 2014" class="img-responsive"><br>
    <img src="{{ asset('frontend/') }}/img/bank 2.jpg" alt="sales 2014" class="img-responsive">
    </div>
    <div class="box">
    <div class="box-header">
    <h4>Jasa Pengiriman Barang</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/JNE.jpg" alt="sales 2014" class="img-responsive">
    </div>
    </div>
    </div>
        <!-- /.container -->
    </div>
    <!-- /#content -->
@endsection
