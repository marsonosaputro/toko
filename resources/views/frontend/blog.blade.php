@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
      <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li>{{ $page_title }}</li>
        </ul>
      </div>
            <!-- *** LEFT COLUMN ***
         _________________________________________________________ -->
    <div class="col-sm-9" id="blog-listing">
      @foreach($artikel as $key => $d)
        <div class="post">
          <h2><a href="{{ url('artikel/'.$d->slug) }}">{{ $d->judul }}</a></h2>
          <p class="author-category">Oleh: {{ $d->user->name }} | Pada: <i class="fa fa-calendar-o"></i> {{ tgl_indo($d->created_at) }} | Dibaca: {{ $d->hits }} kali</p>
          <hr>
          <div class="image" style="float:left; margin-right:10px;">
            <a href="#">
              <img src="{{ asset('images/artikel/'.$d->image) }}" class="img-responsive" alt="Herbal">
            </a>
          </div>
          <p class="intro">{!! str_limit($d->deskripsi, 340) !!}</p>
          <p class="read-more pull-left"><a href="{{ url('artikel/'.$d->slug) }}" class="btn btn-primary">Selengkapnya...</a></p>
          <div style="clear:both;"> </div>
        </div>
      @endforeach
        <div class="pages">
          {!! $artikel->render() !!}
        </div>
    </div>
            <!-- /.col-md-9 -->
            <!-- *** LEFT COLUMN END *** -->
    <div class="col-md-3">
                <!-- *** BLOG MENU ***
_________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">
    <div class="panel-heading">
    <h3 class="panel-title">Kategori Artikel</h3>
    </div>
    <div class="panel-body">
    <ul class="nav nav-pills nav-stacked">
      <li>judul</li>
      <li>judul</li>
    </ul>
    </div>
    </div>
                <!-- /.col-md-3 -->
                <!-- *** BLOG MENU END *** -->
    <div class="box">
    <div class="box-header">
    <h4>Rekening Pembayaran</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/bank 1.jpg" alt="sales 2014" class="img-responsive"><br>
    <img src="{{ asset('frontend/') }}/img/bank 2.jpg" alt="sales 2014" class="img-responsive">
    </div>
    <div class="box">
    <div class="box-header">
    <h4>Jasa Pengiriman Barang</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/JNE.jpg" alt="sales 2014" class="img-responsive">
    </div>
    </div>
    </div>
        <!-- /.container -->
    </div>
@endsection
