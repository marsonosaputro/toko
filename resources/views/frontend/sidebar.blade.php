<div class="panel panel-default sidebar-menu">
  <div class="panel-heading">
    <h3 class="panel-title">Kategori Produk</h3>
  </div>
  <div class="panel-body">
    <ul class="nav nav-pills nav-stacked">
      @foreach($kategoriproduk as $key => $d)
        <li><a href="{{ url('kategori-produk/'.$d->id.'/'.$d->seo) }}">{{ $d->kategori }}</a>
      @endforeach
    </ul>
  </div>
</div>
<!-- *** MENUS AND FILTERS END *** -->
<div class="box">
  <div class="box-header">
    <h4>Rekening Pembayaran</h4>
  </div>
  <img src="{{ asset('frontend/') }}/img/bank 1.jpg" alt="sales 2014" class="img-responsive"><br>
  <img src="{{ asset('frontend/') }}/img/bank 2.jpg" alt="sales 2014" class="img-responsive">
</div>
<div class="box">
  <div class="box-header">
    <h4>Jasa Pengiriman Barang</h4>
  </div>
  <img src="{{ asset('frontend/') }}/img/JNE.jpg" alt="sales 2014" class="img-responsive">
</div>
