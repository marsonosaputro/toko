@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Slideshow</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($slide, ['route' => ['slideshow.update', $slide->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files'=>true]) !!}

          @include('slideshow.form')

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
