@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Kategori Produk</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'kategori.store', 'class' => 'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
              {!! Form::label('kategori', 'Kategori', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('kategori', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('kategori') }}</small>
              </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ route('kategori.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
