@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Kategori Produk</h3>
    </div>
    <div class="panel-body">
      @include('notif')
      <a href="{{ route('kategori.create') }}" class="btn btn-primary" style="margin-bottom:10px;">Tambah</a>

      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Kategori</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($kategori as $d)
              {!! Form::open(['method' => 'delete', 'route' => ['kategori.destroy', $d->id]]) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->kategori }}</td>
                <td>
                  <div class="btn-group">
                    <a href="{{ route('kategori.edit', $d->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> EDIT</a>
                    <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> HAPUS</button>
                  </div>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="pull-right" style="margin-top: -20px;">
        {!! $kategori->render() !!}
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
