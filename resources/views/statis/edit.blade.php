@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Halaman Statis</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($statis, ['route' => ['statis.update', $statis->id], 'files'=>true, 'method' => 'PUT', 'class'=>'form-horizontal']) !!}

          @include('statis.form')

          <div class="btn-group pull-right">
              <a href="{{ url('admin/post') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
