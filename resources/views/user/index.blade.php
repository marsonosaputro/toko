@extends('backend')
@section('content')
  @include('notif')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Data User</h3>
    </div>
    <div class="panel-body">
      <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary" style="margin-bottom:10px;">TAMBAH</a>

      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Lengkap</th>
              <th>Email</th>
              <th>Level User</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user as $d)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->email }}</td>
                <td>{{ $d->role }}</td>
                <td>
                  <a href="{{ url('user/'.$d->id).'/edit' }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> EDIT</a>

                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
