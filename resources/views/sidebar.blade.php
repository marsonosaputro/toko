<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ (isset(Auth::user()->name)) ? Auth::user()->name : 'Admin' }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
<span class="input-group-btn">
  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
</span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ route('identitas.index') }}"><i class="fa fa-university"></i><span>Identitas Web</span></a></li>
            <li><a href="{{ route('artikel.index') }}"><i class="fa fa-font"></i><span>Artikel</span></a></li>
            <li><a href="{{ route('produk.index') }}"><i class="fa fa-gift"></i><span>Produk</span></a></li>
            <li><a href="{{ route('kategori.index') }}"><i class="fa fa-th"></i>Kategori Produk</a></li>
            <li><a href="{{ route('statis.index') }}"><i class="fa fa-tablet"></i>Statis</a></li>
            <li><a href="{{ route('slideshow.index') }}"><i class="fa fa-image"></i>Slideshow</a></li>
            <li><a href="{{ route('banner.index') }}"><i class="fa fa-desktop"></i>Banner</a></li>
            <li><a href="{{ route('user.index') }}"><i class="fa fa-users"></i><span>User</span></a></li>
            <li>
              <a href="{{ url('/logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i><span>Logout</span>
              </a>
            </li>
            <!--
            <li class="treeview">
                <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li>
          -->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
