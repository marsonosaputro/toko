@include('tinymce')
<div class="form-group{{ $errors->has('kategori_id') ? ' has-error' : '' }}">
    {!! Form::label('kategori_id', 'Kategori', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('kategori_id', $kategori, null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('produk') ? ' has-error' : '' }}">
    {!! Form::label('produk', 'Produk', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('produk', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('produk') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
    {!! Form::label('deskripsi', 'Deskripsi', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('deskripsi', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
    {!! Form::label('stock', 'Stock', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::number('stock', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('stock') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
    {!! Form::label('harga', 'Harga', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::number('harga', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('harga') }}</small>
    </div>
</div>

@if (!empty($produk->gambarproduk))
  <div class="form-group">
      {!! Form::label('gambarold', 'Gambar', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        <div class="row">
          @foreach ($produk->gambarproduk as $d)
            <div class="col-md-3">
              <img src="{{ asset('images/produk/'.$d->gambar) }}" class="img img-responsive img-thumbnail" />
            </div>
          @endforeach
        </div>
      </div>
  </div>
@endif

<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
    {!! Form::label('gambar', 'Ganti Gambar', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('gambar[]', ['class' => 'form-control', 'multiple'=>true]) !!}
            <p class="help-block">Ukuran Gambar: </p>
            <small class="text-danger">{{ $errors->first('gambar') }}</small>
        </div>
</div>

{!! Form::hidden('hits', 0) !!}
{!! Form::hidden('user_id', Auth::user()->id) !!}

<div class="btn-group pull-right">
    <a href="{{ route('produk.index') }}" class="btn btn-warning">Batal</a>
    {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
</div>
