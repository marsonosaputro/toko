@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Detail Produk</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <tbody>
            <tr>
              <th>Nama Produk</th><td colspan="3">{{ $produk->produk }}</td>
            </tr>
            <tr>
              <th>Kategori</th><td colspan="3">{{ $produk->kategori->kategori }}</td>
            </tr>
            <tr>
              <th>Stock</th><td colspan="3">{{ $produk->stock }}</td>
            </tr>
            <tr>
              <th>Deskripsi</th><td colspan="3">{!! $produk->deskripsi !!}</td>
            </tr>
            <tr>
              <th>Harga</th><td>Rp. {{ number_format($produk->harga, 0,',','.') }}</td>
            </tr>
            <tr>
              <th>Hits</th><td colspan="3">{{ $produk->hits}}</td>
            </tr>
            <tr>
              <th>Gambar Produk</th><td></td><th></th><td></td>
            </tr>
            <tr>
              @if (!empty($produk->gambarproduk))
                @foreach ($produk->gambarproduk as $d)
                  <td class="col-md-3">
                    <img src="{{ asset('images/produk/'.$d->gambar) }}" class="img img-responsive img-thumbnail" />
                  </td>
                @endforeach
              @endif
            </tr>

          </tbody>
        </table>
      </div>
      <div class="pull-right">
        <div class="btn-group">
          <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-backward"></i> KEMBALI</a>
          <a href="{{ route('produk.edit', $produk->id) }}" class="btn btn-info"><i class="fa fa-edit"></i> EDIT</a>
        </div>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
