@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Produk</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($produk, ['route' => ['produk.update', $produk->id], 'files'=>true, 'class' => 'form-horizontal', 'method' => 'PUT']) !!}

          @include('produk._form')

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
