@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Data Produk</h3>
    </div>
    <div class="panel-body">
      @include('notif')
      <a href="{{ route('produk.create') }}" class="btn btn-primary" style="margin-bottom: 10px;">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Produk</th>
              <th>Kategori</th>
              <th>Stock</th>
              <th>Harga</th>
              <th>Hits</th>
              <th>Admin</th>
              <th style="width: 215px;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($produk as $d)
              {!! Form::open(['method' => 'delete', 'route' => ['produk.destroy', $d->id], 'class' => 'form-horizontal']) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $d->produk }}</td>
                  <td>{{ $d->kategori->kategori }}</td>
                  <td>{{ $d->stock }}</td>
                  <td>{{ number_format($d->harga, 0, ',', '.') }}</td>
                  <td>{{ $d->hits }}</td>
                  <td>{{ $d->user->name }}</td>
                  <td>
                    <div class="btn-group">
                      <a href="{{ route('produk.show', $d->id) }}" class="btn btn-success btn-sm"><i class="fa fa-folder-open-o"></i> VIEW</a>
                      <a href="{{ route('produk.edit', $d->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> EDIT</a>
                      <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" name="button" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> HAPUS</button>
                    </div>
                  </td>
                </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="pull-right" style="margin-top: -20px">
        {!! $produk->render() !!}
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
