<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>
@if (!empty($banner->image))
  <div class="form-group">
      {!! Form::label('gambar', 'Gambar sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
          <img src="{{ URL::asset('images/banner/'.$banner->image) }}" class="img img-responsive img-thumbnail" />
      </div>
  </div>

@endif
<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Gambar Banner', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('image', ['class' => 'form-control']) !!}
            <p class="help-block">Ukuran: 250 x 250px</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
</div>
<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
    {!! Form::label('url', 'URL', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('url') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
    {!! Form::label('publish', 'Publish', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('publish', ['Y'=>'Ya', 'N'=>'Tidak'], null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('publish') }}</small>
    </div>
</div>
