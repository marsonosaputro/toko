@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Daftar Banner</h3>
    </div>
    <div class="panel-body">
      @include('notif')
      <a href="{{ route('banner.create') }}" class="btn btn-primary" style="margin-bottom: 10px;">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>URL</th>
              <th>Gambar</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($banner as $d)
              {!! Form::open(array('url'=>'admin/banner/'.$d->id, 'method'=>'delete')) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->judul }}</td>
                <td>{{ $d->url }}</td>
                <td><img src="{{ URL::asset('/images/banner/'.$d->image) }}" style="width:90px;" class="img img-responsive img-thumbnail" /></td>
                <td>
                  @if ($d->publish == 'Y')
                    <button type="button" class="btn btn-info btn-sm glyphicon glyphicon-check"></button>
                  @else
                    <button type="button" class="btn btn-warning btn-sm glyphicon glyphicon-minus"></button>
                  @endif
                </td>
                <td>
                  <div class="btn-group">
                  <a href="{{ url('admin/banner/'.$d->id.'/edit') }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> EDIT</a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> HAPUS</button>
                  </div>
                </td>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
      {!! $banner->render() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
