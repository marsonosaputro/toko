<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Identitas extends Model
{
  protected $fillable = ['title', 'description', 'keyword', 'author', 'name', 'addres', 'email', 'phone', 'day', 'time'];
}
