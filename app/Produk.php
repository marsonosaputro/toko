<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Kategori;
use App\Gambarproduk;

class Produk extends Model
{
    protected $fillable = ['kategori_id', 'produk', 'slug', 'deskripsi', 'stock', 'harga', 'hits', 'user_id'];

    public function kategori()
    {
      return $this->belongsTo(Kategori::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function gambarproduk()
    {
      return $this->hasMany(Gambarproduk::class);
    }
}
