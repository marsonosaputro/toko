<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $fillable = ['judul', 'slug', 'deskripsi', 'image', 'hits', 'user_id'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
