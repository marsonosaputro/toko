<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Gambarproduk;

class Gambarproduk extends Model
{
    protected $fillable = ['produk_id', 'gambar'];

    public function produk()
    {
      return $this->belongsTo(Produk::class);
    }
}
