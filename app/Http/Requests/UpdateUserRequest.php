<?php

namespace App\Http\Requests;

class UpdateUserRequest extends AddUserRequest
{
    public function rules()
    {
      $rules = parent::rules();
      $rules['email'] = 'required|email|max:255|unique:users,email,'.$this->route('user');
      return $rules;
    }
}
