<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateprodukRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'produk' => 'required|unique:produks,produk,'.$this->route('produk'),
          'deskripsi' => 'required',
          'stock' => 'required',
          'harga' => 'required',
        ];
    }
}
