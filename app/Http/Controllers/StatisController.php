<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Statis;
use Session;
use Image;
use File;

class StatisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = ['judul' => 'required',
                        'content' => 'required'
                        ];

    public function index()
    {
        $data['page_title'] = 'Management konten statis';
        $data['statis'] = Statis::orderBy('id', 'desc')->paginate(10);
        $data['no'] = $data['statis']->firstItem();
        return view('statis.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Management konten statis';
        return view('statis.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if(!empty($request->file('image'))){
            $image = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->move('images/statis/', $image);
            $img = Image::make(public_path().'/images/statis/'.$image)->resize(1000,415);
            $img->save();
        }else{
            $image = '';
        }
        $statis = $request->all();
        $statis['slug'] = str_slug($request['judul'], '-');
        $statis['user_id'] = Auth::user()->id;
        $statis['image'] = $image;
        $statis['hits'] = 0;
        Session::flash('flash_notification', [
                  'level'=>'success',
                  'message'=>'Berhasil menambahkan <b>'.$request['judul'].'</b>'
                ]);
        Statis::create($statis);
        return redirect()->route('statis.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Management Halaman Statis';
        $data['statis'] = Statis::findOrFail($id);
        return view('statis.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, $this->rules);
      $data = Statis::findOrFail($id);
      if(!empty($request->file('image'))){
          $image = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/statis/', $image);
          $img = Image::make(public_path().'/images/statis/'.$image)->resize(1000,415);
          $img->save();
          File::delete(public_path('images/statis/'.$data->image));
      }else{
          $image = $data->image;
      }
      $statis = $request->all();
      $statis['slug'] = str_slug($request['judul'], '-');
      $statis['user_id'] = Auth::user()->id;
      $statis['image'] = $image;
      $statis['hits'] = $data->hits;
      Session::flash('flash_notification', [
                'level'=>'info',
                'message'=>'Berhasil mengubah <b>'.$request['judul'].'</b>'
              ]);
      $data->update($statis);
      return redirect()->route('statis.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $statis = Statis::findOrFail($id);
        Session::flash('flash_notification', [
                  'level'=>'danger',
                  'message'=>'Berhasil menghapus <b>'.$statis['judul'].'</b>'
                ]);
        $statis->delete();
        return redirect()->route('status.index');
    }
}
