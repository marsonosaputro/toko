<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\SimpanprodukRequest;
use App\Http\Requests\UpdateprodukRequest;
use App\Produk;
use App\Kategori;
use App\Gambarproduk;
use Image;
use File;
use Session;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Manajemen Produk';
        $data['produk'] = Produk::paginate(10);
        return view('produk.index', $data)->with('no', $data['produk']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Manajemen Produk';
      $data['kategori'] = Kategori::pluck('kategori', 'id');
      return view('produk.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimpanprodukRequest $request)
    {
        $data = $request->except('gambar');
        $data['slug'] = str_slug($request['produk']);
        $produk = Produk::create($data);

        if(!empty($request->file('gambar')))
        {
          $gambar = $request->file('gambar');
          foreach ($gambar as $key => $d)
          {
            $image = time().$d->getClientOriginalName();
            $d->move('images/produk/', $image);
            Image::make(public_path().'/images/produk/'.$image)->resize(450,600)->save();
            File::copy(public_path().'/images/produk/'.$image, public_path().'/images/produk/thumb/'.$image);
            Image::make(public_path().'/images/produk/thumb/'.$image)->resize(200,200)->save();
            $img = new Gambarproduk;
            $img->gambar = $image;
            $produk->gambarproduk()->save($img);
          }
        }

        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menambah produk <b>'.$request['produk'].'</b>'
              ]);
        return redirect()->route('produk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['page_title'] = 'Manajemen Produk';
        $data['produk'] = Produk::find($id);
        return view('produk.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Manajemen Produk';
        $data['produk'] = Produk::find($id);
        $data['kategori'] = Kategori::pluck('kategori', 'id');
        return view('produk.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateprodukRequest $request, $id)
    {
        $produk = Produk::find($id);
        $data = $request->except('gambar');
        $data['slug'] = str_slug($request['produk']);

        $produk->update($data);

        if(!empty($request->file('gambar')))
        {
          //Hapus Gambar
          foreach ($produk->gambarproduk as $key => $d)
          {
            File::delete(public_path('images/produk/'.$d->gambar));
            File::delete(public_path('images/produk/thumb/'.$d->gambar));
          }
          Gambarproduk::where('produk_id', $id)->delete();

          //Update Gambar
          $gambar = $request->file('gambar');
          foreach ($gambar as $key => $d)
          {
            $image = time().$d->getClientOriginalName();
            $d->move('images/produk/', $image);
            Image::make(public_path().'/images/produk/'.$image)->resize(450,600)->save();
            File::copy(public_path().'/images/produk/'.$image, public_path().'/images/produk/thumb/'.$image);
            Image::make(public_path().'/images/produk/thumb/'.$image)->resize(200,200)->save();
            $img = new Gambarproduk;
            $img->gambar = $image;
            $produk->gambarproduk()->save($img);
          }
        }
        Session::flash('flash_notification', [
                'level'=>'info',
                'message'=>'Berhasil mengubah produk ke <b>'.$request['produk'].'</b>'
              ]);
        return redirect()->route('produk.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        Session::flash('flash_notification', [
                'level'=>'danger',
                'message'=>'Berhasil menghapus produk <b>'.$produk->produk.'</b>'
              ]);
        foreach ($produk->gambarproduk as $key => $d)
        {
          File::delete(public_path('images/produk/'.$d->gambar));
          File::delete(public_path('images/produk/thumb/'.$d->gambar));
        }
        $produk->delete();
        return redirect()->route('produk.index');
    }
}
