<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Artikel;
use Session;
use Image;
use File;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Manajemen Artikel';
        $data['artikel'] = Artikel::orderBy('id', 'desc')->paginate('10');
        return view('artikel.index', $data)->with('no', $data['artikel']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Manajemen Artikel';
        return view('artikel.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'judul' => 'required|unique:artikels,judul',
          'deskripsi' => 'required',
          'images' => 'sometimes|mimes:jpg,jpeg,png'
        ]);

        if(!empty($request->file('image')))
        {
          $img = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/artikel/', $img);
          Image::make(public_path('images/artikel/'.$img))->resize(300,200)->save();
        }
        else
        {
          $img = '';
        }

        $data = $request->all();
        $data['slug'] = str_slug($request['judul']);
        $data['image'] = $img;
        Artikel::create($data);
        Session::flash('flash_notification',[
          'level' => 'success',
          'message' => 'Artikel <b>'.$request['judul'].'</b> berhasil disimpan.'
        ]);
        return redirect(route('artikel.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['page_title'] = 'Manajemen Artikel';
      $data['artikel'] = Artikel::findOrFail($id);
      return view('artikel.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Manajemen Artikel';
      $data['artikel'] = Artikel::findOrFail($id);
      return view('artikel.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'judul' => 'required|unique:artikels,judul,'.$id,
        'deskripsi' => 'required',
        'images' => 'sometimes|mimes:jpg,jpeg,png'
      ]);

      $artikel = Artikel::findOrFail($id);
      if(!empty($request->file('image')))
      {
        $img = time().$request->file('image')->getClientOriginalName();
        $request->file('image')->move('images/artikel/', $img);
        Image::make(public_path('images/artikel/'.$img))->resize(300,200)->save();
        File::delete(public_path('images/artikel/'.$artikel->image));
      }
      else
      {
        $img = $artikel->image;
      }

      $data = $request->all();
      $data['slug'] = str_slug($request['judul']);
      $data['image'] = $img;
      Session::flash('flash_notification',[
        'level' => 'info',
        'message' => 'Mengubah artikel <b>'.$artikel->judul.'</b> ke <b>'.$request['judul'].'</b>'
      ]);
      $artikel->update($data);
      return redirect(route('artikel.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artikel = Artikel::findOrFail($id);
        Session::flash('flash_notification',[
          'level' => 'danger',
          'message' => 'Berhasil menghapus artikel <b>'.$artikel->judul.'</b>'
        ]);
        File::delete(public_path('images/artikel/'.$artikel->image));
        $artikel->delete();
        return redirect(route('artikel.index'));
    }
}
