<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Statis;
use App\Produk;
use App\Artikel;
use App\Slideshow;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $data['slideshow'] = Slideshow::where('publish', 'Y')->take(4)->get();
        $data['produk'] = Produk::orderBy('id', 'desc')->paginate(6);
        $data['artikel'] = Artikel::orderBy('id', 'desc')->take(2)->get();
        return view('frontend.home', $data);
    }

    public function about()
    {
      $data['page_title'] = 'Profil Website';
      $data['statis'] = Statis::where('id', 1)->first();
      DB::table('statis')->where('id',1)->update(['hits'=> $data['statis']->hits + 1]);
      return view('frontend.statis', $data);
    }

    public function caraBelanja()
    {
      $data['page_title'] = 'Cara Belanja';
      $data['statis'] = Statis::where('id', 2)->first();
      DB::table('statis')->where('id',2)->update(['hits'=> $data['statis']->hits + 1]);
      return view('frontend.statis', $data);
    }

    public function kontak()
    {
      $data['page_title'] = 'Kontak kami';
      return view('frontend.kontak', $data);
    }

    public function produk($slug='')
    {
      if (!empty($slug)) {
        $data['detail'] = Produk::where('slug', $slug)->first();
        DB::table('produks')->where('slug',$slug)->update(['hits'=> $data['detail']->hits + 1]);
        $data['terbaru'] = Produk::orderBy('id', 'desc')->take(3)->get();
        $data['hits'] = Produk::orderBy('hits', 'desc')->take(3)->get();
        $data['page_title'] = 'Produk Kami';
        return view('frontend.detailproduk', $data);
      }
      else
      {
        $data['page_title'] = 'Produk Kami';
        $data['produk'] = Produk::orderBy('id', 'desc')->paginate(6);
        return view('frontend.galleryProduk', $data);
      }
    }

    function artikel($slug='')
    {
      if (!empty($slug))
      {
        $data['artikel'] = Artikel::where('slug', $slug)->first();
        DB::table('artikels')->where('slug',$slug)->update(['hits'=> $data['artikel']->hits + 1]);
        $data['page_title'] = 'Artikel | '.$data['artikel']->judul;
        return view('frontend.artikel', $data);
      }
      else
      {
        $data['page_title'] = 'Artikel';
        $data['artikel'] = Artikel::orderBy('id', 'desc')->paginate(6);
        return view('frontend.blog', $data);
      }
    }
}
