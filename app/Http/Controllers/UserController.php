<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use App\Role;
use Session;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management User';
        $data['user'] = User::paginate(10);
        return view('user.index', $data)->with('no', $data['user']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Management User';
      $data['role'] = Role::pluck('display_name', 'name');
      return view('user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
         $user = User::create([
           'name' => $request['name'],
           'email' => $request['email'],
           'password' => bcrypt($request['password']),
         ]);
         $role = Role::where('name', $request['role'])->first();
         $user->attachRole($role);
         Session::flash('flash_notification', [
           'level' => 'success',
           'message' => 'User '.$request['name'].' berhasil ditambahkan dengan level '.$request['role'],
         ]);
         return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        $data['role'] = Role::pluck('display_name', 'id');
        return view('user.edit', $data)->with('page_title', 'Management User');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->update([
          'name' => $request['name'],
          'email' => $request['email'],
          'password' => bcrypt($request['password']),
        ]);
        DB::table('role_user')->where('user_id', $id)->update(['role_id'=>$request['role']]);
        Session::flash('flash_notification', [
          'level' => 'info',
          'message' => 'Data user berhasil diubah ',
        ]);
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
