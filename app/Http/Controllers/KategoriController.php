<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Kategori;
use Session;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Manajemen Kategori Produk';
        $data['kategori'] = Kategori::paginate(10);
        return view('kategori.index', $data)->with('no', $data['kategori']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Manajemen Kategori Produk';
      return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'kategori' => 'required|unique:kategoris,kategori'
        ]);
        Kategori::create(['kategori'=>$request['kategori'], 'slug'=>str_slug($request['kategori'])]);
        Session::flash('flash_notification',[
          'level' => 'success',
          'message' => 'Kategori <b>'.$request['kategori'].'</b> berhasil ditambahkan'
        ]);
        return redirect()->route('kategori.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Manajemen Kategori Produk';
      $data['kategori'] = Kategori::find($id);
      return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'kategori' => 'required|unique:kategoris,kategori,'.$id
      ]);
      $kategori = Kategori::find($id);
      Session::flash('flash_notification',[
        'level' => 'info',
        'message' => 'Kategori <b>'.$kategori->kategori .'</b> berhasil diedit menjadi kategori <b>'.$request['kategori'].'</b>'
      ]);
      $kategori->update(['kategori'=>$request['kategori'], 'slug'=>str_slug($request['kategori'])]);
      return redirect()->route('kategori.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kategori = Kategori::find($id);
      Session::flash('flash_notification',[
        'level' => 'danger',
        'message' => 'Kategori <b>'.$kategori->kategori .'</b> berhasil dihapus '
      ]);
      $kategori->delete();
      return redirect()->route('kategori.index');
    }
}
