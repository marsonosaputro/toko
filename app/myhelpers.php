<?php

if ( ! function_exists('hari_ini'))
{
	function hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
        $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
        $hari = date("w");
        $hari_ini = $seminggu[$hari];
        return $hari_ini;
	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tanggal)
	{
		$tgl = explode(' ', $tanggal);
		$t   = explode('-', $tgl[0]);
	    switch ($t[1])
	        {
	            case '01'  : $b = 'Jan'; break;
	            case '02'  : $b = 'Feb'; break;
	            case '03'  : $b = 'Mar'; break;
	            case '04'  : $b = 'Apr'; break;
	            case '05'  : $b = 'Mei'; break;
	            case '06'  : $b = 'Jun'; break;
	            case '07'  : $b = 'Jul'; break;
	            case '08'  : $b = 'Agt'; break;
	            case '09'  : $b = 'Sep'; break;
	            case '10'  : $b = 'Okt'; break;
	            case '11'  : $b = 'Nop'; break;
	            case '12'  : $b = 'Des'; break;
	        }
	    $d = $t[2].' '.$b.' '.$t[0];
	    return $d.' | '.$tgl[1];
	}
}

if ( ! function_exists('tanggal'))
{
	function tanggal($tanggal)
	{
		$t   = explode('-', $tanggal);
	    switch ($t[1])
	        {
	            case '01'  : $b = 'Jan'; break;
	            case '02'  : $b = 'Feb'; break;
	            case '03'  : $b = 'Mar'; break;
	            case '04'  : $b = 'Apr'; break;
	            case '05'  : $b = 'Mei'; break;
	            case '06'  : $b = 'Jun'; break;
	            case '07'  : $b = 'Jul'; break;
	            case '08'  : $b = 'Agt'; break;
	            case '09'  : $b = 'Sep'; break;
	            case '10'  : $b = 'Okt'; break;
	            case '11'  : $b = 'Nop'; break;
	            case '12'  : $b = 'Des'; break;
	        }
	    $d = $t[2].' '.$b.' '.$t[0];
	    return $d;
	}
}

function gambar($id='')
{
	$d = DB::table('gambarproduks')->where('produk_id', $id)->first();
	return $d->gambar;
}

function gambar2($id='')
{
	$d = DB::table('gambarproduks')->where('produk_id', $id)->latest()
                ->first();
	return $d->gambar;
}
